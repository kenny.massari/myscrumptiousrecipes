from django.urls import path
from .views import list_recipes, create_recipe, show_recipe, edit_recipe, user_recipes



urlpatterns = [
    path("", list_recipes, name = "recipes_list"),
    path("create/", create_recipe, name = "create_recipe"),
    path("<int:id>/", show_recipe, name = "show_recipe"),
    path("<int:id>/edit/", edit_recipe, name = "edit_recipe"),
    path("mine/", user_recipes, name = "user_recipes"),
]
