from django.shortcuts import get_object_or_404, render, redirect
from recipes.models import Recipe
from .forms import RecipeForm
from django.contrib.auth.decorators import login_required

@login_required
def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()
            return redirect("recipes_list")
    else:
        form = RecipeForm()
    context = {"form": form}
    return render(request, "recipes/create.html", context)

def edit_recipe(request, id):
    recipe = Recipe.objects.get(id=id)
    form = RecipeForm(instance=recipe)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect('recipes_list')

    context = {
        "recipe": recipe,
        'form':form
    }
    return render(request, "recipes/edit.html", context)

def list_recipes(request):
    really_cool_recipes = Recipe.objects.all()
    context = {
        "recipes": really_cool_recipes
    }
    return render(
        request,
        "recipes/list.html", context
    )
@login_required
def user_recipes(request):
    my_recipes = Recipe.objects.filter(author = request.user)
    context = {
        "recipe_list": my_recipes,
    }
    return render(request, "recipes/list.html", context)

def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipes_object": recipe,
    }
    return render(request, "recipes/detail.html", context)
